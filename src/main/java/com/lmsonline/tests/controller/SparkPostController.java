package com.lmsonline.tests.controller;

import java.util.ArrayList;
import java.util.List;


import com.sparkpost.Client;
import com.sparkpost.exception.SparkPostException;
import com.sparkpost.model.*;
import com.sparkpost.model.responses.Response;
import com.sparkpost.resources.ResourceTransmissions;
import com.sparkpost.transport.IRestConnection;
import com.sparkpost.transport.RestConnection;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;



@Service
public class SparkPostController {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass());


    @Value("${sparkpost_api}")
    private String API_KEY;

    @Value("${text}")
    private String text;

    @Value("${bcc_recipients}")
    private List<String> bcc_recipients;


    private Client client = null;


    public void sendEmail(String from, String recipient, String message, String subject, String attachment) throws SparkPostException {
        TransmissionWithRecipientArray transmission = new TransmissionWithRecipientArray();

        // Populate Recipients
        List<RecipientAttributes> recipientArray = new ArrayList<>();

        // Primary 'To' recipients
        RecipientAttributes recipientAttribs = new RecipientAttributes();
        AddressAttributes addressAttribs = new AddressAttributes(recipient);
        recipientAttribs.setAddress(addressAttribs);
        recipientArray.add(recipientAttribs);

        // Secondary 'BCC' recipients with the primary recipients listed in 'To:' header
        String toHeader = recipient;
        for (String bcc : bcc_recipients) {
            RecipientAttributes recipientAttri = new RecipientAttributes();
            AddressAttributes addressAttri = new AddressAttributes(bcc);
            addressAttri.setHeaderTo(toHeader);
            recipientAttri.setAddress(addressAttri);
            recipientArray.add(recipientAttri);
        }

        transmission.setRecipientArray(recipientArray);

      //  attachment.replace("\"","\\\"");
       // attachment.replace("\n","\\n");
        AttachmentAttributes attachmentAttributes = new AttachmentAttributes();
        attachmentAttributes.setData(attachment);
        attachmentAttributes.setType("text/csv");
        attachmentAttributes.setName("attachment.csv");
        List<AttachmentAttributes> attachment1 = new ArrayList<>();
        attachment1.add(attachmentAttributes);

        // Populate Email Body
        TemplateContentAttributes contentAttributes = new TemplateContentAttributes();
        contentAttributes.setFrom(new AddressAttributes(from));
        contentAttributes.setSubject(subject);
        contentAttributes.setText(text);
        contentAttributes.setHtml(message);
        contentAttributes.setAttachments(attachment1);

        transmission.setContentAttributes(contentAttributes);

        if (client == null) {
            client = new Client(API_KEY);
        }
        // Send the Email
        IRestConnection connection = new RestConnection(client);
        Response response = ResourceTransmissions.create(connection, 0, transmission);

        log.debug("Transmission Response: " + response);

    }

}
