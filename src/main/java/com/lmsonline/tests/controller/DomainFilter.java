package com.lmsonline.tests.controller;

import java.util.List;

/**
 * Created by raul on 8/11/17.
 */
public class DomainFilter {

    public static void main(String[] args) {
        String email = "example@lmsonline.com"; // or a parameter, wherever this comes from

        // chopIndex will be -1 if '@' is not present so look for this error condition
// if there is no '@' it is not a valid email so probably some Exception
// (maybe IllegalArgumentException) needs to be thrown
// otherwise chopIndex is the index of the first place '@' occurs.
        int chopIndex = email.indexOf('@');

        // Add 1 to the chopIndex since we want what starts right after '@'
// Also convert to lowercase right away
        String domain = email.substring(chopIndex + 1).toLowerCase();

        // Make sure all of these are lowercase
        List<String> matchGroup1 = java.util.Arrays.asList("yahoo.com", "google.com", "aol.com");
        // Create a list even if there is just one so others could be added later
        List<String> matchGroup2 = java.util.Arrays.asList("lmsonline.com");
// additional matching groups if needed ....

        if (matchGroup1.contains(domain)) {
            System.out.print("personalemail");
        }

        else if (matchGroup2.contains(domain)) {
            System.out.print("workemail");


        } else {

            System.out.print("nothing changes");
        }

        // etc
    }
}