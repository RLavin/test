package com.lmsonline.tests.controller;

import com.lmsonline.tests.data.Email;
import com.lmsonline.tests.mail.MyBuilder;
import com.sparkpost.exception.SparkPostException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/lms")
public class EmailController {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SparkPostController sparkPostController;
    @Autowired
    private MyBuilder mailContentBuilder = null;

    @Value("${from_email}")
    private String from_email;

    @Value("${subject}")
    private String subject;

    /**
     * Saves a new email to the database and autogenerates and Id
     * Also sends the email to the desire recipient
     * @param aEmail
     * @return
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "email", method = RequestMethod.POST, produces = "application/json")
    public void email(@RequestBody Email aEmail) throws SparkPostException {
        String msg = mailContentBuilder.buildRepEmail(aEmail.getName(),aEmail.getEmail(), aEmail.getAge());
        sparkPostController.sendEmail(from_email,aEmail.getEmail(),msg,subject, aEmail.getAttachment());
        log.info("Sent email to: " + aEmail.getEmail());



    }


}
