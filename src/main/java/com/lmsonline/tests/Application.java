package com.lmsonline.tests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 *This class initializes the SpringBootApplication, fires it up.
 */
@EnableSwagger2
@SpringBootApplication
public class Application   {
    public static void main(String[] args){

        SpringApplication.run(Application.class,args);
    }

    @Bean
    public Docket EmailApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("email-api")
                .apiInfo(EmailapiInfo())
                .select()
                .paths(regex("/lms.*"))
                .build();

    }


    private ApiInfo EmailapiInfo() {
        return new ApiInfoBuilder()
                .title("Brevera Email Service")
                .description("Email Interested Brevera Customers")
                .version("1.0")
                .build();
    }


}
