package com.lmsonline.tests.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;


/**
 * This class is responsible for preparing mail content
 * by combining the written template and external model
 */
@Service
public class MailContentBuilder implements MyBuilder {

    private TemplateEngine templateEngine = null;

    @Autowired
    public MailContentBuilder(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;

    }



    public String buildRepEmail(String name, String email, int age) {
        Context context = new Context();
        context.setVariable("name", name);
        context.setVariable("email", email);
        context.setVariable("age", age);
        return templateEngine.process("example", context);


    }


}