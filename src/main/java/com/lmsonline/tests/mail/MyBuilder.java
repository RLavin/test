package com.lmsonline.tests.mail;


public interface MyBuilder {
    public String buildRepEmail(String name, String email, int age);
}
